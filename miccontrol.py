#!/usr/bin/env python

import speechrecognition


def main():
    Microphone(device_index: Union[int, None] = None, sample_rate: int = 16000, chunk_size: int = 1024) -> Microphone
    return Microphone

